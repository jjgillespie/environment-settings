# My Environment Settings

## Ubuntu

* Used rbenv for ruby
* Used nvm for node

#### TMUX

* Clone tmux plugin manager via `git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm` [https://github.com/tmux-plugins/tpm](https://github.com/tmux-plugins/tpm)
* Copy the `.tmux.conf` file to home
* Run tmux
* Type `Ctrl+a` + `I` to install the plugins listed in the tmux.conf file.

##### Notable Keyboard Shortcuts
* `Ctrl+a` + `Ctrl+a` - Open previous window
* `Ctrl+a` + `-` - Split vertically
* `Ctrl+a` + `|` - Split horizontally
* `Ctral+a` + `[arrows]` - Move focus to pane above/below/left/right the current pane
* `Ctrl+a` + `c` - Create new window
* `Ctrl+a` + `,` - Rename current window

#### Bash

Copy `.bash_profile` and `.bashrc` to home


## Git

* Copy `.gitconfig` to home (user folder on windows)
* Windows - use Https and Git Credential Manager - [https://github.com/Microsoft/Git-Credential-Manager-for-Windows](https://github.com/Microsoft/Git-Credential-Manager-for-Windows)

## Linters

#### Project Level Settings

* Copy `sass-lint.yml` to project root
* Copy `coffeelint.json` to project root

## Sublime

* I'm not sure if I edited the sublime linter settings but I copied my current file into `linters/SublimeLinter.sublime-settings` just in case
* I used Adobe's Source Code Pro font which worked out pretty well [https://github.com/adobe-fonts/source-code-pro](https://github.com/adobe-fonts/source-code-pro)

#### Mac
* Add sym link for sublime to `subl` command:
	`ln -s /Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl /usr/local/bin/subl`

#### Packages

* Better CoffeeScript
* BracketHighlighter
* Color Highlighter
* Emmet
* JavaScriptNext - ES6 Syntax (?? Not sure how helpful this is ??)
* Markdown Preview
* RSpec
* rspec-snippets
* Sass
* Side Bar Enhancements
* SublimeLinter [http://sublimelinter.readthedocs.org/en/latest/installation.html](http://sublimelinter.readthedocs.org/en/latest/installation.html)
* SublimeLinter-coffeelint [https://github.com/SublimeLinter/SublimeLinter-coffeelint](https://github.com/SublimeLinter/SublimeLinter-coffeelint)
* SublimeLinter-contrib-sass-lint [https://github.com/skovhus/SublimeLinter-contrib-sass-lint](https://github.com/skovhus/SublimeLinter-contrib-sass-lint)
* SublimeLinter-ruby [https://github.com/SublimeLinter/SublimeLinter-ruby](https://github.com/SublimeLinter/SublimeLinter-ruby)
* Tag
* Theme - SoDaReloaded
* GitGutter

Good article about sublime and rails (https://mattbrictson.com/sublime-text-3-recommendations)