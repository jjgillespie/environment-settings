{
  "bold_folder_labels": true,
  "caret_style": "phase",
  "color_scheme": "Packages/User/SublimeLinter/Monokai Soda (SL).tmTheme",
  "detect_indentation": false,
  "detect_slow_plugins": false,
  "draw_white_space": "all",
  "fade_fold_buttons": false,
  "folder_exclude_patterns":
  [
    ".svn",
    ".git",
    ".hg",
    "CVS",
    "/dist",
    "/dist/*",
    "node_modules",
    "public"
  ],
  "font_face": "Source Code Pro Medium",
  "font_options":
  [
    "gray_antialias",
    "subpixel_antialias"
  ],
  "font_size": 10,
  "highlight_line": true,
  "hot_exit": true,
  "ignored_packages":
  [
    "Markdown",
    "Vintage"
  ],
  "line_numbers": true,
  "line_padding_bottom": 1,
  "line_padding_top": 1,
  "shift_tab_unindent": true,
  "soda_classic_tabs": true,
  "soda_folder_icons": true,
  "tab_completion": true,
  "tab_size": 2,
  "theme": "SoDaReloaded Dark.sublime-theme",
  "translate_tabs_to_spaces": true,
  "trim_trailing_white_space_on_save": true,
  "word_wrap": false
}
